package com.crater.barcodegenerator;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(Include.NON_NULL)
public class EncodedData {
	
	private final Byte workshopId;
	private final Long orderId;
	private final Long itemId;
	
	@SuppressWarnings("unused")
	private EncodedData() {
		this(null, null, null);
	}

	public EncodedData(final Byte workshopId, final Long orderId, final Long itemId) {
		this.workshopId = workshopId;
		this.orderId = orderId;
		this.itemId = itemId;
	}
	
	public String getDataPattern(){
		return String.valueOf(workshopId+"_"+orderId+"_"+itemId).trim();
	}

	@JsonProperty("workshop_id")
	public Byte getWorkshopId() {
		return workshopId;
	}

	@JsonProperty("order_id")
	public Long getOrderId() {
		return orderId;
	}

	@JsonProperty("item_id")
	public Long getItemId() {
		return itemId;
	}
	
}
