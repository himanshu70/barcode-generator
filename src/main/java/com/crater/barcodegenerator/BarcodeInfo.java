package com.crater.barcodegenerator;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;


@JsonInclude(Include.NON_NULL)
public class BarcodeInfo {

	private final String title;
	private final String itemId;
	private final String pin;
	private final String locality;
	private final String date;
	private final String priority;
	private final String acronym;
	private final String name;
	private final String serviceNumber;
	private final String totalService;
	
	
	@SuppressWarnings("unused")
	private BarcodeInfo() {
		this(null, null, null, null, null, null, null, null, null, null);
	}

	public BarcodeInfo(final String title, final String itemId, final String pin, final String locality,
			final String date, final String priority, final String acronym,
			final String name, final String serviceNumber, final String totalService) {
		this.title = title;
		this.itemId = itemId;
		this.pin = pin;
		this.locality = locality;
		this.date = date;
		this.priority = priority;
		this.acronym = acronym;
		this.name = name;
		this.serviceNumber = serviceNumber;
		this.totalService = totalService;
	}
	
	@JsonProperty("title")
	public String getTitle() {
		return title;
	}

	@JsonProperty("item_id")
	public String getItemId() {
		return itemId;
	}

	@JsonProperty("pin")
	public String getPin() {
		return pin;
	}

	@JsonProperty("locality")
	public String getLocality() {
		return locality;
	}

	@JsonProperty("date")
	public String getDate() {
		return date;
	}

	@JsonProperty("priority")
	public String getPriority() {
		return priority;
	}

	@JsonProperty("name")
	public String getName() {
		return name;
	}

	@JsonProperty("service_number")
	public String getServiceNumber() {
		return serviceNumber;
	}

	@JsonProperty("acronym")
	public String getAcronym() {
		return acronym;
	}

	@JsonProperty("total_service_in_order")
	public String getTotalService() {
		return totalService;
	}
	
}
