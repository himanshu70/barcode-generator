package com.crater.barcodegenerator;

import java.awt.image.BufferedImage;

import org.krysalis.barcode4j.BarcodeDimension;
import org.krysalis.barcode4j.TextAlignment;
import org.krysalis.barcode4j.impl.code128.Code128Bean;
import org.krysalis.barcode4j.output.bitmap.BitmapCanvasProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class BarcodeGenerator {

	private final Code128Bean barcode;
	
	@Autowired
	public BarcodeGenerator(final Code128Bean barcode) {
		this.barcode =barcode;
	}

	public BufferedImage generateBarcodeWithInfo(final EncodedData data, final BarcodeInfo barInfo){
		final BitmapCanvasProvider canvasProvider = new BitmapCanvasProvider(150,
				BufferedImage.TYPE_BYTE_BINARY, false, 0);
		barcode.generateBarcode(canvasProvider, data.getDataPattern());
		drawImage(canvasProvider, barInfo, barcode.getFontName());
		return canvasProvider.getBufferedImage();
	}
	
	public BufferedImage generateBarcode(final EncodedData data){
		final BitmapCanvasProvider canvasProvider = new BitmapCanvasProvider(150,
				BufferedImage.TYPE_BYTE_BINARY, false, 0);
		barcode.generateBarcode(canvasProvider, data.getDataPattern());
		return canvasProvider.getBufferedImage();
	}
	
	public void drawImage(final BitmapCanvasProvider canvas, final BarcodeInfo bar,
			final String fontName){
		
		final BarcodeDimension bd = canvas.getDimensions();
		
		canvas.deviceText(bar.getName()+" | "+bar.getLocality()+" | "+bar.getPin(), bd.getWidthPlusQuiet(), 0, 18, fontName,
                8, TextAlignment.TA_CENTER);
        canvas.deviceText(bar.getItemId()+" | "+bar.getAcronym()+" | "+bar.getServiceNumber()+"/"+bar.getTotalService()+" | "+bar.getDate(), bd.getWidthPlusQuiet(), 0, bd.getHeight()+27d, fontName,
                8, TextAlignment.TA_CENTER);
	}
}
